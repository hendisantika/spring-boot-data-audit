package com.hendisantika.springbootdataaudit;

import com.hendisantika.springbootdataaudit.Repo.UserRepository;
import com.hendisantika.springbootdataaudit.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootDataAuditApplicationTests {

    @Autowired
    private UserRepository userRepository;

    private User user;

    @Before
    public void create() {
        user = userRepository.save(
                new User().setName("Uzumaki Naruto").setUsername("naruto")
        );

        assertThat(user.getCreated())
                .isNotNull();

        assertThat(user.getModified())
                .isNotNull();

        assertThat(user.getCreatedBy())
                .isEqualTo("Mr. Auditor");

        assertThat(user.getModifiedBy())
                .isEqualTo("Mr. Auditor");
    }

    @Test
    public void update() {
        LocalDateTime created = user.getCreated();
        LocalDateTime modified = user.getModified();

        userRepository.save(
                user.setUsername("rashidi")
        );

        userRepository.findById(user.getId())
                .ifPresent(updatedUser -> {

                    assertThat(updatedUser.getUsername())
                            .isEqualTo("rashidi");

                    assertThat(updatedUser.getCreated())
                            .isEqualTo(created);

                    assertThat(updatedUser.getModified())
                            .isAfter(modified);
                });
    }

}
